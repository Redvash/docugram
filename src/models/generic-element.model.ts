import { ElementType } from '@/types';

export class GenericElementModel {
  public id: string;
  public type: ElementType;
  public positionX: number;
  public positionY: number;
  public width: number;
  public height: number;

  constructor (data?: GenericElementModel) {
    this.id = data?.id || (new Date()).getMilliseconds().toString();
    this.type = data?.type || ElementType.Container;
    this.positionX = data?.positionX || 10;
    this.positionY = data?.positionY || 10;
    this.width = data?.width || 200;
    this.height = data?.height || 100;
  }

  public getSaveableObject (): GenericElementModel {
    return new GenericElementModel(this);
  }

  public isSearchRelevant (searchText: string): boolean {
    return false;
  }
}

import { GenericElementModel } from '@/models/generic-element.model';
import { TextElementModel } from '@/models/text-element.model';
import { ElementType } from '@/types';

export class ContainerElementModel extends GenericElementModel {
  public children: GenericElementModel[];
  public name: string;
  public parent?: ContainerElementModel;

  constructor (data?: ContainerElementModel) {
    super(data);

    this.children = data?.children || [];
    this.name = data?.name || 'New container';
    this.parent = data?.parent || undefined;
    this.type = ElementType.Container;

    this.children = this.children.map(child => {
      let parsedChild: GenericElementModel;

      if (child.type === ElementType.Container) {
        parsedChild = new ContainerElementModel(child as ContainerElementModel);
        (parsedChild as ContainerElementModel).parent = this;
      }
      else if (child.type === ElementType.Text) {
        parsedChild = new TextElementModel(child as TextElementModel);
      }
      else {
        parsedChild = new GenericElementModel(child);
      }

      return parsedChild;
    });
  }

  get breadcrumbs (): string {
    let breadcrumbs = this.name;
    let currentParent = this.parent;

    while (currentParent) {
      breadcrumbs = `${currentParent.name}  \\  ${breadcrumbs}`;
      currentParent = currentParent.parent;
    }

    return breadcrumbs;
  }

  get description (): string {
    const firstTextElement = this.children.find(child => child.type === ElementType.Text);

    if (firstTextElement) {
      return (firstTextElement as TextElementModel).content;
    }

    return '';
  }

  public getSaveableObject (): ContainerElementModel {
    const clone = new ContainerElementModel(this);

    delete clone.parent;
    clone.children = clone.children.map(child => child.getSaveableObject());

    return clone;
  }

  public isSearchRelevant (searchText: string): boolean {
    const searchTerms = searchText.split(' ');

    return searchTerms.every(term => this.name.indexOf(term) !== -1) ||
      this.children.some(child => child.isSearchRelevant(searchText));
  }
}

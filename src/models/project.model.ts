import { GenericElementModel } from '@/models/generic-element.model';

export class ProjectModel {
  public name: string;
  public elements: GenericElementModel[];

  constructor (data?: ProjectModel) {
    this.name = data?.name || 'New Project';
    this.elements = data?.elements || [];
  }
}

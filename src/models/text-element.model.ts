import { GenericElementModel } from '@/models/generic-element.model';
import { ElementType } from '@/types';

export class TextElementModel extends GenericElementModel {
  public content: string;

  constructor (data?: TextElementModel) {
    super(data);

    this.content = data?.content || '';
    this.type = ElementType.Text;
  }

  public getSaveableObject (): TextElementModel {
    return new TextElementModel(this);
  }

  public isSearchRelevant (searchText: string): boolean {
    const searchTerms = searchText.split(' ');

    return searchTerms.every(term => this.content.toLowerCase().indexOf(term) !== -1);
  }
}

import Vue from 'vue';
import Vuex from 'vuex';

import { ElementType } from '@/types';

// Models
import { ContainerElementModel } from '@/models/container-element.model';
import { GenericElementModel } from '@/models/generic-element.model';
import { ProjectModel } from '@/models/project.model';

Vue.use(Vuex);

function flattenedElements (state: { project: ProjectModel }): GenericElementModel[] {
  const elements: ContainerElementModel[] = [];
  processElements(state.project.elements, elements);
  return elements;

  function processElements (elements: GenericElementModel[], elementsList: GenericElementModel[]) {
    elements.forEach(element => {
      elementsList.push(element);

      if (element.type === ElementType.Container) {
        processElements((element as ContainerElementModel).children, elementsList);
      }
    });
  }
}

export default new Vuex.Store({
  state: {
    project: new ProjectModel(),
    searchText: null
  },

  getters: {
    flattenedElements
  },

  mutations: {
    addElement (state, { element, containerId }) {
      // If container id is given. find given container and add element there
      // Otherwise, add it to root container
      if (containerId) {
        const container = flattenedElements(state)
          .filter((element: GenericElementModel) => element.type === ElementType.Container)
          .find((container: GenericElementModel) => container.id === containerId);

        if (container) {
          (container as ContainerElementModel).children.push(element);
        }
      }
      else {
        state.project.elements.push(element);
      }
    },

    setProject (state, { project }) {
      state.project = project;
    },

    setProjectName (state, { projectName }) {
      state.project.name = projectName;
    },

    setSearchText (state, { searchText }) {
      state.searchText = searchText;
    }
  },

  actions: {
    addElement ({ commit, dispatch }, { element, containerId }) {
      dispatch('setSearchText', {
        clear: true
      });
      commit('addElement', {
        element,
        containerId
      });
    },

    loadProject ({ commit }, { project }) {
      commit('setProject', {
        project
      });
    },

    setProjectName ({ commit }, { projectName }) {
      commit('setProjectName', {
        projectName
      });
    },

    setSearchText ({ commit }, { searchText, clear }) {
      if (clear || searchText === '') {
        commit('setSearchText', {
          searchText: null
        });
      }
      else {
        commit('setSearchText', {
          searchText
        });
      }
    }
  }
});

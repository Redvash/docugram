import Vue from 'vue';
import App from './App.vue';
import store from './store';

// Font Awesome
import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/js/all.min.js';

import CanvasLayer from '@/components/canvas-layer.component.vue';

Vue.component('canvas-layer', CanvasLayer);

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
